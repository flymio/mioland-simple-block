<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

#$arParams["GET"] = $_GET;
if($arParams["USE_FILTER"]=="Y")
{
	if(strlen($arParams["FILTER_NAME"])<=0 || !ereg("^[A-Za-z_][A-Za-z01-9_]*$", $arParams["FILTER_NAME"]))
		$arParams["FILTER_NAME"] = "arrFilter";
}
else
	$arParams["FILTER_NAME"] = "";

$arDefaultUrlTemplates404 = array(
	"sections" => "",
	"section" => "#SECTION_ID#/",
	"element" => "#SECTION_ID#/#ELEMENT_ID#/",
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array(
	"SECTION_ID",
	"SECTION_CODE",
	"ELEMENT_ID",
	"ELEMENT_CODE",
	"action",
);

$arSort = array();


if (!$arParams['SORT']){
  $arParams['SORT'] = "SORT";
}

$arSort['ARRAY'] = array("SORT"=>"DESC","NAME"=>"ASC");

if ($arParams['SORT_PARAM'] && $arParams['SORT_VARIANTS']){
  if (isset($_REQUEST['desc'])){
    $arSort['DESC'] = 1;
  }
  if (in_array($_REQUEST[$arParams['SORT_PARAM']],$arParams['SORT_VARIANTS'])){
    $arSort['KEY'] = $_REQUEST[$arParams['SORT_PARAM']];
    if ($arParams['SORT_REPLACE'] && $arParams['SORT_REPLACE'][$_REQUEST[$arParams['SORT_PARAM']]]){
      $_REQUEST[$arParams['SORT_PARAM']] = $arParams['SORT_REPLACE'][$_REQUEST[$arParams['SORT_PARAM']]];
    }
    if (!in_array($_REQUEST[$arParams['SORT_PARAM']],array("NAME","SORT"))){
      $_REQUEST[$arParams['SORT_PARAM']] = "PROPERTY_".strtoupper($_REQUEST[$arParams['SORT_PARAM']]);
    }
    $arSort['ARRAY'] = array($_REQUEST[$arParams['SORT_PARAM']] => $arSort['DESC'] ? "DESC":"ASC");
  }
}

if (empty($arParams['PAGE_ELEMENT_COUNT'])){
  $arParams['PAGE_ELEMENT_COUNT'] = 10;
}

$arNavParams = array(
  "nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
  "bShowAll" => 'Y',
  "iNumPage" => $_GET['PAGEN_1']
);
$arNavigation = CDBResult::GetNavParams($arNavParams);
$NAV_STRING = '';

$url = "";
if (empty($arParams["CACHE_KEYS"]) || preg_match("/REDIRECT_URL/",$arParams["CACHE_KEYS"])){
  $arParams["CACHE_KEYS"]=str_replace("REDIRECT_URL","",$arParams["CACHE_KEYS"]);
  if ($_SERVER['REDIRECT_URL']) {
    $url = $_SERVER["REDIRECT_URL"];  
  }
  else{
    $url = $_SERVER["REQUEST_URI"];
    if (preg_match("/clear_cache/",$url)){
      $url=str_replace("?clear_cache=Y","",$url);
      $url=str_replace("&clear_cache=Y","",$url);
    }
  }
  $ADDITIONAL_CACHE_ID[] = $url;
}

foreach(explode(",", $arParams["CACHE_KEYS"]) as $value){
  if ($value){
    if ($_SERVER[$value]){
      $ADDITIONAL_CACHE_ID[] = $_SERVER[$value];
    }
    else if ($_REQUEST[$value]){
      $ADDITIONAL_CACHE_ID[] = $_REQUEST[$value];
    }
    else if ($_COOKIE[$value]){
      $ADDITIONAL_CACHE_ID[] = $_COOKIE[$value];
    }
    else if ($GLOBALS[$value]){
      $ADDITIONAL_CACHE_ID[] = $GLOBALS[$value];
    }
  }
}

$sections=array(); 
$items=array();
if($arParams["SEF_MODE"] == "Y")
{


  $arVariables = array();

  $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
  $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

  $engine = new CComponentEngine($this);
  if (CModule::IncludeModule('iblock'))
  {
          $engine->addGreedyPart("#SECTION_CODE_PATH#");
          $engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
  }
  $componentPage = $engine->guessComponentPath(
          $arParams["SEF_FOLDER"],
          $arUrlTemplates,
          $arVariables
  );



	$temp_url = $url;
	$temp_url = str_replace($arParams["SEF_FOLDER"],"",$temp_url);
	$url_parts = explode("/", $temp_url);
	if (empty($url_parts[0]) || $url_parts[0] == ''){
  	$url_parts=array();
	}
	$arVariablesNew = array();
	if (count($url_parts)>0){
  	foreach($url_parts as $part) {
    	$arVariablesNew[]=$part;
  	}
	}
	if (count($url_parts)>2){
	  for($i=0;$i<=count($url_parts)-2;$i++){
  	  array_shift($url_parts);
	  }
	  $temp_url = implode("/",$url_parts);
	  $url = $arParams["SEF_FOLDER"].$temp_url;
	}

	$count = 0;
	$arVariables["PARTS"] = $arVariablesNew;

	if(!$componentPage)
		$componentPage = "sections";

	CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

  if ($componentPage == 'element'){
      $section = array();
      CModule::IncludeModule("iblock");
      
      if ($arVariables['SECTION_CODE']){
        $arFilter = Array(
          'IBLOCK_ID' => $arParams["IBLOCK_ID"],
          'ACTIVE'=>'Y',
          "CODE" => $arVariables['SECTION_CODE']
        );
        $section_res = CIBlockSection::GetList($arSort['ARRAY'], $arFilter, true, array("*", "UF_*"));

        $section = $section_res->GetNext();
        $arVariables['SECTION_ID'] = $section['ID'];
        $arVariables['SECTION'] = $section;
      }
      elseif ($arVariables['SECTION_ID']){
        $arFilter = Array(
          'IBLOCK_ID' => $arParams["IBLOCK_ID"],
          'ACTIVE'=>'Y',
          "ID" => $arVariables['SECTION_ID']
        );
        $section_res = CIBlockSection::GetList($arSort['ARRAY'], $arFilter, true, array("*", "UF_*"));
        $section = $arVariables['SECTION'] = $section_res->GetNext();
      }
      $arFilter = Array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ACTIVE" => "Y",
      );
      if ($section['ID']){
        $arFilter["SECTION_ID"] = $section['ID'];
      }
      if ($arVariables['ELEMENT_CODE']){
        $arFilter["CODE"] = $arVariables['ELEMENT_CODE'];
      }
      if ($arVariables['ELEMENT_ID']){
        $arFilter["ID"] = $arVariables['ELEMENT_ID'];
      }

      $rsIBlock = CIBlockElement::GetList($arSort['ARRAY'], $arFilter, false,array(),array("NAME","CODE","ID","PREVIEW_PICTURE","DETAIL_PICTURE","PREVIEW_TEXT","DETAIL_PAGE_URL","DETAIL_TEXT"));
      $NAV_STRING = $rsIBlock->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);

      
      while($arElement=$rsIBlock->GetNext()){
      	$db_props = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement['ID'], "sort", "asc", array());
        while($ar_props = $db_props->Fetch()){
          if (!$ar_props['VALUE']) continue;
          if ($ar_props['MULTIPLE'] == 'Y'){
            $arElement[$ar_props['CODE'] .($ar_props['PROPERTY_TYPE'] == 'F' ? "_FILE":"")][] = $ar_props['PROPERTY_TYPE'] == 'F' ? array("DESC"=>$ar_props['DESCRIPTION'],"FILE"=>$ar_props['VALUE']) : $ar_props['VALUE'];
          }
          else{
            $arElement[$ar_props['CODE'].($ar_props['PROPERTY_TYPE'] == 'F' ? "_FILE":"")] = $ar_props['PROPERTY_TYPE'] == 'F' ? array("DESC"=>$ar_props['DESCRIPTION'],"FILE"=>$ar_props['VALUE']) : $ar_props['VALUE'];
          }
        }
        $item=$arElement;


        $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arParams["IBLOCK_ID"], $item["ID"]);
        $arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();
        
        if ($arResult["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"] != "")
          $APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"], $arTitleOptions);
        elseif(isset($item['NAME']))
          $APPLICATION->SetTitle($item['NAME'], $arTitleOptions); 

        $metaKeywords = \Bitrix\Main\Type\Collection::firstNotEmpty(
            $arResult["PROPERTIES"], array($arParams["ELEMENT_META_KEYWORDS"], "VALUE")
            ,$arResult["IPROPERTY_VALUES"], "ELEMENT_META_KEYWORDS"
        );
        
        
        if (is_array($metaKeywords))
            $APPLICATION->SetPageProperty("keywords", implode(" ", $metaKeywords), $arTitleOptions);
        elseif ($metaKeywords != "")
            $APPLICATION->SetPageProperty("keywords", $metaKeywords, $arTitleOptions);
        
        $metaDescription = \Bitrix\Main\Type\Collection::firstNotEmpty(
            $arResult["PROPERTIES"], array($arParams["ELEMENT_META_DESCRIPTION"], "VALUE")
            ,$arResult["IPROPERTY_VALUES"], "ELEMENT_META_DESCRIPTION"
        );
        if (is_array($metaDescription))
            $APPLICATION->SetPageProperty("description", implode(" ", $metaDescription), $arTitleOptions);
        elseif ($metaDescription != "")
            $APPLICATION->SetPageProperty("description", $metaDescription, $arTitleOptions);
        
      }
      
  }
  
  if ($componentPage == 'section'){
      CModule::IncludeModule("iblock");
      
      if ($arVariables['SECTION_ID'] && !isset($arVariables['SECTION'])){
        $arFilter = Array(
          'IBLOCK_ID' => $arParams["IBLOCK_ID"],
          'ACTIVE'=>'Y',
          "ID" => $arVariables['SECTION_ID']
        );
        $section_res = CIBlockSection::GetList($arSort['ARRAY'], $arFilter, true, array("*","UF_*"));
        $section = $section_res->GetNext();
        $arVariables['SECTION'] = $section;
      }

      $arFilter = Array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ACTIVE" => "Y",
      );
      if ($section){
        $arFilter["SECTION_ID"] = $section['ID'];
      }


      $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arFilter["SECTION_ID"]);
      $arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();
      

      if ($arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"] != "")
        $APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"], $arTitleOptions);
      elseif(isset($arVariables['SECTION']['NAME']))
        $APPLICATION->SetTitle($arVariables['SECTION']['NAME'], $arTitleOptions); 

      $metaKeywords = \Bitrix\Main\Type\Collection::firstNotEmpty(
          $arResult["PROPERTIES"], array($arParams["SECTION_META_KEYWORDS"], "VALUE")
          ,$arResult["IPROPERTY_VALUES"], "SECTION_META_KEYWORDS"
      );
      
      
      if (is_array($metaKeywords))
          $APPLICATION->SetPageProperty("keywords", implode(" ", $metaKeywords), $arTitleOptions);
      elseif ($metaKeywords != "")
          $APPLICATION->SetPageProperty("keywords", $metaKeywords, $arTitleOptions);
      
      $metaDescription = \Bitrix\Main\Type\Collection::firstNotEmpty(
          $arResult["PROPERTIES"], array($arParams["SECTION_META_DESCRIPTION"], "VALUE")
          ,$arResult["IPROPERTY_VALUES"], "SECTION_META_DESCRIPTION"
      );
      if (is_array($metaDescription))
          $APPLICATION->SetPageProperty("description", implode(" ", $metaDescription), $arTitleOptions);
      elseif ($metaDescription != "")
          $APPLICATION->SetPageProperty("description", $metaDescription, $arTitleOptions);
            

      $rsIBlock = CIBlockElement::GetList($arSort['ARRAY'], $arFilter, false,$arNavParams,array("NAME","CODE","ID","PREVIEW_PICTURE","DETAIL_PICTURE","PREVIEW_TEXT","DETAIL_PAGE_URL","active_from"));
      $NAV_STRING = $rsIBlock->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
      
      while($arElement=$rsIBlock->GetNext())
      {
      	$db_props = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement['ID'], "sort", "asc", array());
        while($ar_props = $db_props->Fetch()){
          if (!$ar_props['VALUE']) continue;
          if ($ar_props['MULTIPLE'] == 'Y'){
            $arElement[$ar_props['CODE'] .($ar_props['PROPERTY_TYPE'] == 'F' ? "_FILE":"")][] = $ar_props['PROPERTY_TYPE'] == 'F' ? array("DESC"=>$ar_props['DESCRIPTION'],"FILE"=>$ar_props['VALUE']) : $ar_props['VALUE'];
          }
          else{
            $arElement[$ar_props['CODE'].($ar_props['PROPERTY_TYPE'] == 'F' ? "_FILE":"")] = $ar_props['PROPERTY_TYPE'] == 'F' ? array("DESC"=>$ar_props['DESCRIPTION'],"FILE"=>$ar_props['VALUE']) : $ar_props['VALUE'];
          }
        }
      	$items[]=$arElement;
      }
  }

  if ($componentPage == 'sections' || $componentPage == 'section'){
      CModule::IncludeModule("iblock");
  
      $arFilter = Array(
        'IBLOCK_ID' => $arParams["IBLOCK_ID"],
        'ACTIVE'=>'Y',
        "SECTION_ID" => false
      );
      if ($arVariables['SECTION_ID']){
        $arFilter['SECTION_ID'] = $arVariables['SECTION_ID'];
      }
      $section_res = CIBlockSection::GetList($arSort['ARRAY'], $arFilter, true, array("*","UF_*"));
      while($section = $section_res->GetNext())
      {
        $sections[]=$section;
      }
      
      if ($arParams["LOAD_SUBITEMS"] && $arParams["LOAD_SUBITEMS"] == "Y"){
       
        
        $sections_load = array();
        foreach($sections as $section){
          $sections_load[]=$section['ID'];
        }
        $sections_load[]= $arVariables['SECTION_ID'];

        $items=array();
        $arFilter = Array(
          'IBLOCK_ID' => $arParams["IBLOCK_ID"],
          'ACTIVE'=>'Y',
          "SECTION_ID" => $sections_load
        );       
        
        //debug($arFilter);
        $rsIBlock = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false,false,array("NAME","CODE","ID","PREVIEW_PICTURE","DETAIL_PICTURE","PREVIEW_TEXT","DETAIL_PAGE_URL","active_from"));
        $NAV_STRING = $rsIBlock->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);

        while($arElement=$rsIBlock->GetNext())
        {

        	$db_props = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement['ID'], "sort", "asc", array());
          while($ar_props = $db_props->Fetch()){
            if (!$ar_props['VALUE']) continue;
            if ($ar_props['MULTIPLE'] == 'Y'){
              $arElement[$ar_props['CODE'] .($ar_props['PROPERTY_TYPE'] == 'F' ? "_FILE":"")][] = $ar_props['PROPERTY_TYPE'] == 'F' ? array("DESC"=>$ar_props['DESCRIPTION'],"FILE"=>$ar_props['VALUE']) : $ar_props['VALUE'];
            }
            else{
              $arElement[$ar_props['CODE'].($ar_props['PROPERTY_TYPE'] == 'F' ? "_FILE":"")] = $ar_props['PROPERTY_TYPE'] == 'F' ? array("DESC"=>$ar_props['DESCRIPTION'],"FILE"=>$ar_props['VALUE']) : $ar_props['VALUE'];
            }
          }
        	$items[]=$arElement;
        }
        
      }
      
      if (sizeof($sections)<1 && $componentPage == 'sections'){
        if ($arVariables['SECTION_CODE'] && !isset($arVariables['SECTION'])){
          $arFilter = Array(
            'IBLOCK_ID' => $arParams["IBLOCK_ID"],
            'ACTIVE'=>'Y',
            "CODE" => $arVariables['SECTION_CODE']
          );
          $section_res = CIBlockSection::GetList($arSort['ARRAY'], $arFilter, true, array("*","UF_*"));
          $section = $section_res->GetNext();
          $arVariables['SECTION_ID'] = $section['ID'];
          $arVariables['SECTION'] = $section;
        }
  
        $arFilter = Array(
          "IBLOCK_ID" => $arParams["IBLOCK_ID"],
          "ACTIVE" => "Y",
        );
        if ($section){
          $arFilter["SECTION_ID"] = $section['ID'];
        }
  
        $rsIBlock = CIBlockElement::GetList($arSort['ARRAY'], $arFilter, false,$arNavParams,array("NAME","CODE","ID","PREVIEW_PICTURE","DETAIL_PICTURE","PREVIEW_TEXT","DETAIL_PAGE_URL"));
        $NAV_STRING = $rsIBlock->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);

        while($arElement=$rsIBlock->GetNext())
        {
        	$db_props = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement['ID'], "sort", "asc", array());
          while($ar_props = $db_props->Fetch()){
            if (!$ar_props['VALUE']) continue;
            if ($ar_props['MULTIPLE'] == 'Y'){
              $arElement[$ar_props['CODE'] .($ar_props['PROPERTY_TYPE'] == 'F' ? "_FILE":"")][] = $ar_props['PROPERTY_TYPE'] == 'F' ? array("DESC"=>$ar_props['DESCRIPTION'],"FILE"=>$ar_props['VALUE']) : $ar_props['VALUE'];
            }
            else{
              $arElement[$ar_props['CODE'].($ar_props['PROPERTY_TYPE'] == 'F' ? "_FILE":"")] = $ar_props['PROPERTY_TYPE'] == 'F' ? array("DESC"=>$ar_props['DESCRIPTION'],"FILE"=>$ar_props['VALUE']) : $ar_props['VALUE'];
            }
          }
        	$items[]=$arElement;
        }
      }
  }
  
  
  $iblock = CIBlock::GetByID($arParams["IBLOCK_ID"]);  
  $iblock_info = $iblock->Fetch();
  
  //breadcrumbs
  $arBreadcrumbs = array();
  if (!empty($arVariables['SECTION'])){
    $arFilter = Array(
      "IBLOCK_ID" => $arParams["IBLOCK_ID"],
      "ACTIVE" => "Y",
      "<=LEFT_BORDER" => $arVariables['SECTION']["LEFT_MARGIN"],
      ">=RIGHT_BORDER" => $arVariables['SECTION']["RIGHT_MARGIN"]
    );
    $arSelect = array("ID", "NAME", "DEPTH_LEVEL", "LEFT_MARGIN", "RIGHT_MARGIN", "SECTION_PAGE_URL");
    $rsIBlock = CIBlockSection::GetList(array("LEFT_MARGIN"=>"ASC"), $arFilter, false, $arSelect);
    while($arSection=$rsIBlock->GetNext()){
      $arBreadcrumbs[] = $arSection;
    }
  }

  //siblings
  $arSiblings = array();
  if (isset($arParams['IS_SIBLINGS']) && $arParams['IS_SIBLINGS'] == 'Y' && !empty($arVariables['SECTION']['IBLOCK_SECTION_ID'])){
    $arFilter = Array(
      "IBLOCK_ID" => $arParams["IBLOCK_ID"],
      "ACTIVE" => "Y",
      "SECTION_ID" => $arVariables['SECTION']["IBLOCK_SECTION_ID"]
    );
    $arSelect = array("ID", "NAME", "CODE", "SORT", "PICTURE", "DETAIL_PICTURE", "DESCRIPTION", "DESCRIPTION_TYPE", "SECTION_PAGE_URL", "UF_*");
    $rsIBlock = CIBlockSection::GetList(array("SORT"=>"ASC"), $arFilter, false, $arSelect);
    while($arSection=$rsIBlock->GetNext()){
      $arSiblings[] = $arSection;
    }
  }
  $user = array();
  if ($USER){
    $user['id'] = $USER->GetID();
    $user['group'] = $USER->GetUserGroupArray();
  }
  
	$arResult = array(
		"FOLDER" => $arParams["SEF_FOLDER"],
    "NAV_STRING" => $NAV_STRING,
		"URL_TEMPLATES" => $arUrlTemplates,
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases,
    "BREADCRUMBS" => $arBreadcrumbs,
    "SIBLINGS" => $arSiblings,
		"SECTIONS" => $sections,
		"ITEMS" => $items,
		"SECTION" => $arVariables['SECTION'],
		"ITEM" => $item,
		"IBLOCK" => $iblock_info,
		"SORT" => $arSort,
	);


}
else
{
	$arVariables = array();

	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
	CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

	$componentPage = "";

	if(isset($arVariables["ELEMENT_ID"]) && intval($arVariables["ELEMENT_ID"]) > 0)
		$componentPage = "element";
	elseif(isset($arVariables["ELEMENT_CODE"]) && strlen($arVariables["ELEMENT_CODE"]) > 0)
		$componentPage = "element";
	elseif(isset($arVariables["SECTION_ID"]) && intval($arVariables["SECTION_ID"]) > 0)
		$componentPage = "section";
	elseif(isset($arVariables["SECTION_CODE"]) && strlen($arVariables["SECTION_CODE"]) > 0)
		$componentPage = "section";
	else
		$componentPage = "sections";


  if ($componentPage == 'sections'){
    CModule::IncludeModule("iblock");
      $arFilter = Array(
        'IBLOCK_ID' => $arParams["IBLOCK_ID"],
        'ACTIVE'=>'Y',
        "SECTION_ID" => false
      );
      $section_res = CIBlockSection::GetList($arSort['ARRAY'], $arFilter, true);
      while($section = $section_res->GetNext())
      {
        $sections[]=$section;
      }
  }
  
	$arResult = array(
		"FOLDER" => "",
		"URL_TEMPLATES" => Array(
			"section" => htmlspecialchars($APPLICATION->GetCurPage())."?".$arVariableAliases["SECTION_ID"]."=#SECTION_ID#",
			"element" => htmlspecialchars($APPLICATION->GetCurPage())."?".$arVariableAliases["SECTION_ID"]."=#SECTION_ID#"."&".$arVariableAliases["ELEMENT_ID"]."=#ELEMENT_ID#",
		),
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases,
		"SECTIONS" => $sections,
		"SORT" => $arSort
	);
}

$CACHE_PATH = "/".SITE_ID."/".LANGUAGE_ID.$this->__relativePath;
$ADDITIONAL_CACHE_ID_STRING = implode(";", $ADDITIONAL_CACHE_ID);

$ar_cache_key = array_merge($ADDITIONAL_CACHE_ID, $arNavigation, $arSort);

if ($_REQUEST['clear_cache']){
  $this->ClearResultCache($ADDITIONAL_CACHE_ID_STRING, $CACHE_PATH);
  //$this->IncludeComponentTemplate($componentPage);
  $this->AbortResultCache();
}
else if ($_SERVER["HTTP_METHOD"] == "POST"){
  $this->ClearResultCache($ADDITIONAL_CACHE_ID_STRING, $CACHE_PATH);
  //$this->IncludeComponentTemplate($componentPage);
  $this->AbortResultCache();
}
if($this->StartResultCache($arParams["CACHE_TIME"], $ar_cache_key, $CACHE_PATH)) 
{
  $this->IncludeComponentTemplate($componentPage);
}

// Подключаем файл без кеширования
$modifier_path = $_SERVER["DOCUMENT_ROOT"].$arResult["__TEMPLATE_FOLDER"]."/result_modifier_nc.php";
$modifier_path_print = $arResult["__TEMPLATE_FOLDER"]."/result_modifier_nc.php";
$modifier_short_path = $_SERVER["DOCUMENT_ROOT"].$arResult["__TEMPLATE_FOLDER"]."/nc.php";

if (file_exists($modifier_short_path))
{
	require_once($modifier_short_path);
	$mod_name = "nc.php";
}
elseif (file_exists($modifier_path))
{
	require_once($modifier_path);
	$mod_name = "result_modifier_nc.php";
}
// Подключаем шаблон без кеширования
$nocahe_template_path = $_SERVER["DOCUMENT_ROOT"].$arResult["__TEMPLATE_FOLDER"]."/template_nc.php";
if (file_exists($nocahe_template_path))
{
	require_once($nocahe_template_path);
}

return $arResult;
?>