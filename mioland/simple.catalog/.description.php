<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Пустышка-каталог",
	"DESCRIPTION" => "Простой компонент каталога для создания любой логики",
	"ICON" => "/images/catalog.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "utility",
	),
);

?>