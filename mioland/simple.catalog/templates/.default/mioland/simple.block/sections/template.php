<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["SECTIONS"]):?>
<ul>
<?foreach($arResult["SECTIONS"] as $section):?>
  <li><a href="<?=$section["SECTION_PAGE_URL"]?>"><?=$section["NAME"]?></a></li>
<?endforeach?>
</ul>
<?endif?>

<?if($arResult["ITEMS"]):?>
  GOT ITEMS3:<br/>
  <?foreach($arResult["ITEMS"] as $item):?>
    &nbsp;&nbsp;&nbsp;<a href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a><br/>
  <?endforeach?>
<?endif?>
