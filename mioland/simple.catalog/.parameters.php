<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();
$arIBlock=array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch())
{
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}

$arAscDesc = array(
	"asc" => GetMessage("IBLOCK_SORT_ASC"),
	"desc" => GetMessage("IBLOCK_SORT_DESC"),
);


if ($arCurrentValues["IBLOCK_ID"]){
  $arSortVariants = array('NAME' => GetMessage('NAME_OF_ELEMENT'));
  $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID"]));
  while ($prop_fields = $properties->GetNext())
  {
    $arSortVariants[$prop_fields['CODE']]=$prop_fields['NAME'];
  }
}

$arComponentParameters = array(
	"PARAMETERS" => array(
		"VARIABLE_ALIASES" => Array(
			"SECTION_ID" => Array("NAME" => GetMessage("SECTION_ID_DESC")),
			"ELEMENT_ID" => Array("NAME" => GetMessage("ELEMENT_ID_DESC")),
		),
		"SORT_PARAM" => array(
		  "NAME" => GetMessage('SORT_PARAM'),
		  "TYPE" => "STRING",
		  "DEFAULT" => "SORT",
		),
		"SORT_VARIANTS" => array(
		  "NAME" => GetMessage('SORT_VARIANTS'),
		  "TYPE" => "LIST",
		  "MULTIPLE" => "Y",
		  "SIZE" => 10,
		  "VALUES" => $arSortVariants,
		  "DEFAULT" => "NAME",
		),
		"SORT_SHOW" => array(
		  "NAME" => GetMessage('SORT_SHOW'),
		  "TYPE" => "LIST",
		  "VALUES" => array(
		    "NONE" =>  GetMessage('SORT_SHOW_NONE'),
		    "TOP" =>  GetMessage('SORT_SHOW_TOP'),
		    "BOTTOM" =>  GetMessage('SORT_SHOW_BOTTOM'),
		    "ALL" =>  GetMessage('SORT_SHOW_ALL')		    
		  ),
		  "DEFAULT" => "NONE",
		),

		"SEF_MODE" => Array(
			"sections" => array(
				"NAME" => GetMessage("SECTIONS_TOP_PAGE"),
				"DEFAULT" => "",
				"VARIABLES" => array(),
			),
			"section" => array(
				"NAME" => GetMessage("SECTION_PAGE"),
				"DEFAULT" => "",
				"VARIABLES" => array("SECTION_ID"=>"SID"),
			),
			"element" => array(
				"NAME" => GetMessage("DETAIL_PAGE"),
				"DEFAULT" => "#SECTION_ID#/#ELEMENT_ID#",
				"VARIABLES" => array("ELEMENT_ID"=>"EID"),
			),
		),
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlockType,
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_IBLOCK"),
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "Y",
			"VALUES" => $arIBlock,
			"REFRESH" => "Y",
		),
		"PAGE_ELEMENT_COUNT" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("ELEMENT_COUNTS"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "10",
		),
		"IS_SIBLINGS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IS_SIBLINGS"),
			"TYPE" => "CHECKBOX",
			"MULTIPLE" => "N",
			"DEFAULT" => "N"
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
		"CACHE_KEYS"  =>  Array("DEFAULT" => "REDIRECT_URL","NAME" => GetMessage("CACHE_KEYS")),
	),
);
CIBlockParameters::AddPagerSettings($arComponentParameters, '', true, true);
?>