<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"CACHE_TIME"  =>  array("DEFAULT"=>3600),
		"CACHE_KEYS"  =>  array("DEFAULT"=>"REDIRECT_URL", "NAME"=>"Ключи для кеширования"),
		"LOAD" => array("TYPE"=>"CHECKBOX","DEFAULT"=>"N", "NAME"=>"Загрузить данные"),
		"LOAD_EXT" => array("TYPE"=>"CHECKBOX","DEFAULT"=>"N", "NAME"=>"Загрузить внешние данные"),
		"LOAD_EXT_PARAMS" => array("DEFAULT"=>"NAME,CODE,ID", "NAME"=>"Параметры внешних данных"),
		"FILTER" => array("DEFAULT"=>"", "NAME"=>"Фильтр элементов")		
	),
);
?>
