<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arComponentDescription = array(
	"NAME" => "Пустышка-список",
	"DESCRIPTION" => "Простой компонент для создания любой сложной логики",
	"ICON" => "/images/menu_ext.gif",
	"PATH" => array(
		"ID" => "utility",
	),
);
?>