<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// inc counter text here
if ($arResult["ITEMS"] && count($arResult["ITEMS"])>0){
  foreach($arResult["ITEMS"] as $key => $value){
    CIBlockElement::CounterInc($value["ID"]);
  }
}
else if ($arResult["ITEM"]){
  CIBlockElement::CounterInc($arResult["ITEM"]["ID"]);
}
else if ($arResult["ID"]){
  CIBlockElement::CounterInc($arResult["ID"]);
}
?>