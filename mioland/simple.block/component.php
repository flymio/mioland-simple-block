<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if (empty($arParams["CACHE_KEYS"]) || preg_match($arParams["CACHE_KEYS"],"REDIRECT_URL",$arParams["CACHE_KEYS"])){
  $arParams["CACHE_KEYS"]=str_replace("REDIRECT_URL","",$arParams["CACHE_KEYS"]);
  if ($_SERVER['REDIRECT_URL']) {
    $url = $_SERVER["REDIRECT_URL"];  
  }
  else{
    $url = $_SERVER["REQUEST_URI"];
    if (preg_match("/clear_cache/",$url)){
      $url=str_replace("?clear_cache=Y","",$url);
      $url=str_replace("&clear_cache=Y","",$url);
    }
  }
  $ADDITIONAL_CACHE_ID[] = $url;
}
foreach(explode(",", $arParams["CACHE_KEYS"]) as $value){
  if ($value){
    if ($_SERVER[$value]){
      $ADDITIONAL_CACHE_ID[] = $_SERVER[$value];
    }
    else if ($_REQUEST[$value]){
      $ADDITIONAL_CACHE_ID[] = $_REQUEST[$value];
    }
    else if ($_COOKIE[$value]){
      $ADDITIONAL_CACHE_ID[] = $_COOKIE[$value];
    }
  }
}

if ($arParams["RESULT"]){
  $arResult = $arParams["RESULT"];
}
$items = array();

if ($arParams["LOAD"]){
  CModule::IncludeModule("iblock");
  $arFilter = Array(
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "ACTIVE" => "Y"
  );
  if ($arParams['SECTION_ID']){
    
    $arFilter['ID'] = $arParams['SECTION_ID'];

    $rsIBlockSection = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, true, array("NAME","CODE","ID","PREVIEW_PICTURE","DETAIL_PICTURE","PREVIEW_TEXT","DETAIL_PAGE_URL","DETAIL_TEXT","SECTION_PAGE_URL","UF*"));
    $arElement=$rsIBlockSection->GetNext();
    $arResult['SECTION'] = $arElement;

    unset($arFilter['ID']);
    $arFilter['SECTION_ID'] = $arParams['SECTION_ID'];
  }
  if ($arParams["FILTER"]){
    foreach($GLOBALS[$arParams["FILTER"]] as $key => $value){
       $arFilter[$key] = $value;
    }
  }
  $arSort = Array("SORT" => "ASC");
  if ($arParams["SORT"]){
    $arSort = $arParams["SORT"];
  }
  $rsIBlock = CIBlockElement::GetList(
    $arSort, 
    $arFilter, 
    false,
    false,
    array("NAME","CODE","ID","PREVIEW_PICTURE","DETAIL_PICTURE","PREVIEW_TEXT","DETAIL_PAGE_URL","DETAIL_TEXT","SHOW_COUNTER","DATE_ACTIVE_FROM","DATE_ACTIVE_TO")
  );
  $counter = 0;
  $needLoading = array();
  $needLoading_yet = array();
  while($arElement=$rsIBlock->GetNext())
  {
  	$db_props = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arElement['ID'], "sort", "asc", array());
    while($arProps = $db_props->Fetch()){
    
      //debug($arProps);
      if ($arParams["LOAD_EXT"]){
        if ($arProps['PROPERTY_TYPE'] == 'E'){ // get information about external list
          $key = $arProps['LINK_IBLOCK_ID'].$counter.$arProps['CODE'];
          if (!$needLoading_yet[$key]){
            $needLoading_yet[$key] = 1;
            $needLoading[$arProps['LINK_IBLOCK_ID']][] = array($counter, $arProps['CODE']);
          }
        }
        if ($arProps['PROPERTY_TYPE'] == 'F'){ // get information about external list
          
        }
      }
      
      if (!isset($arElement[$arProps['CODE']])){
        $arElement[$arProps['CODE']] = $arProps['VALUE'];
      }
      else{
        if (!is_array($arElement[$arProps['CODE']])){
          $temp = $arElement[$arProps['CODE']];
          $arElement[$arProps['CODE']]=null;
          $arElement[$arProps['CODE']][] = $temp;
        }
        $arElement[$arProps['CODE']][] = $arProps['VALUE'];
      }

      $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arElement["IBLOCK_ID"], "CODE"=>$arProps['CODE'], 'ID' => $arProps['VALUE']));
      while($enum_fields = $property_enums->GetNext())
      {
        $arElement[$arProps['CODE']."_LIST"][$arProps['VALUE']]=$enum_fields['VALUE'];
      }  
    }
  	$items[]=$arElement;
  	$counter++;
  }
  $arSelectExt = array("NAME","ID","CODE");
  if ($arParams['LOAD_EXT_PARAMS']){
    $arSelectExt=array();
    foreach(explode(",",$arParams['LOAD_EXT_PARAMS']) as $key){
      $arSelectExt[]=$key;
    }
  }
  $other_items = array();
  if ($arSelectExt){
    $arFilter = array("IBLOCK_ID" => array_keys($needLoading));
      $rsIBlock = CIBlockElement::GetList(
        Array("SORT" => "ASC"), 
        $arFilter, 
        false,
        false,
        $arSelectExt
      );
      while($arElement=$rsIBlock->GetNext())
      {
      	$db_props = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement['ID'], "sort", "asc", array());
        while($arProps = $db_props->Fetch()){
          if (!isset($arElement[$arProps['CODE']])){
            $arElement[$arProps['CODE']] = $arProps['VALUE'];
          }
          else{
            if (!is_array($arElement[$arProps['CODE']])){
              $temp = $arElement[$arProps['CODE']];
              $arElement[$arProps['CODE']]=null;
              $arElement[$arProps['CODE']][] = $temp;
            }
            $arElement[$arProps['CODE']][] = $arProps['VALUE'];
          }
        }
      	$other_items[$arElement["ID"]]=$arElement;
      	$counter++;
    }
    
    
    foreach($needLoading as $iblock_id => $item){
      foreach($item as $k => $v){
        if (is_array($items[$k][$v[1]])){
          foreach($items[$k][$v[1]] as $num => $v1){
            $items[$k][$v[1]][$num]=$other_items[$v1];
          }
        }
        else{
          $items[$k][$v[1]]=$other_items[$items[$k][$v[1]]];
        }
      }
    }
  }

  $arResult["ITEMS"]=$items;
}

$CACHE_PATH = "/".SITE_ID."/". LANGUAGE_ID. $this->__relativePath;
$ADDITIONAL_CACHE_ID_STRING = implode(";", $ADDITIONAL_CACHE_ID);

if ($_REQUEST['clear_cache']){
  $this->ClearResultCache($ADDITIONAL_CACHE_ID_STRING, $CACHE_PATH);
  $this->AbortResultCache();
  $this->IncludeComponentTemplate();
}
// including cache area (result_modifier.php)
else if($this->StartResultCache($arParams["CACHE_TIME"], $ADDITIONAL_CACHE_ID_STRING, $CACHE_PATH))
{  
	$this->IncludeComponentTemplate();
}
// including Nocache File
$modifier_path = $_SERVER["DOCUMENT_ROOT"].$arResult["__TEMPLATE_FOLDER"]."/result_modifier_nc.php";

$modifier_path_print = $arResult["__TEMPLATE_FOLDER"]."/result_modifier_nc.php";
if (file_exists($modifier_path))
	require_once($modifier_path);

return $arResult;
?>
